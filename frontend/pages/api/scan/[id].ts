// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { getUser } from '@/lib/users.server';
import { HttpStatus } from '@/lib/HttpStatus';
import { getServerSession } from 'next-auth';
import { authOptions } from '../auth/[...nextauth]';
import { hashEmail } from '@/lib/users';
import { createScan } from '@/lib/scans.server';
import { Scan } from '@/lib/Scan';
import { bingoProperties } from '@/lib/BingoBoard';

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Scan | undefined>
) {
    const { query, method, body } = req;
    // Email hash of the person being scanned
    const scanneeEmailHash = query.id as string;
    const session = await getServerSession(req, res, authOptions);
    const scannerEmail = session?.user?.email;
    if (!scannerEmail) {
        res.status(HttpStatus.FORBIDDEN).json(undefined);
        res.end();
        return;
    }
    const scannerEmailHash = hashEmail(scannerEmail).toString();

    if (method === "PUT") {
        const property = body.property as string;
        return handlePut(res, scannerEmailHash, scanneeEmailHash, property);
    } else {
        res.setHeader('Allow', ['PUT']);
        res.status(HttpStatus.METHOD_NOT_ALLOWED).end(`Method ${method} not allowed`);
        res.end();
        return Promise.resolve();
    }
}

async function handlePut(
    res: NextApiResponse<Scan | undefined>,
    scannerEmailHash: string,
    scanneeEmailHash: string,
    property: string
) {
    const scannee = await getUser(scanneeEmailHash);
    if (!scannee) {
        res.status(HttpStatus.NOT_FOUND).json(undefined);
        res.end();
        return;
    }
    const scanner = await getUser(scannerEmailHash);
    if (!scanner) {
        res.status(HttpStatus.NOT_FOUND);
        res.end();
        return;
    }

    const bingoProperty = bingoProperties[property];
    if (bingoProperty === undefined) {
        res.status(HttpStatus.BAD_REQUEST).json(undefined);
        res.end();
        return;
    }
    
    if (!bingoProperty.matches(scannee, scanner)) {
        res.status(HttpStatus.BAD_REQUEST);
        res.end();
        return;
    }
    // TODO: ensure that you can't scan the same person twice.

    const scan = {scanner: scannerEmailHash, scannee: scanneeEmailHash, property};
    const result = await createScan(scan);
    if (!result) {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(undefined);
        res.end();
        return
    }
    res.status(HttpStatus.CREATED).json({...scan, id: result.insertedId});
    res.end();
}