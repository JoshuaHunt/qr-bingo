import NextAuth from "next-auth"
import EmailProvider from "next-auth/providers/email"
import { MongoDBAdapter } from "@next-auth/mongodb-adapter"
import mongoClient from "../../../lib/mongo"

export const authOptions = {
  adapter: MongoDBAdapter(mongoClient),
  providers: [
    EmailProvider({
      server: process.env.EMAIL_SERVER,
      from: process.env.EMAIL_FROM,
    }),
  ],
}
export default NextAuth(authOptions)