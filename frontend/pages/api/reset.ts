import { HttpStatus } from "@/lib/HttpStatus";
import { deleteScans } from "@/lib/scans.server";
import { hashEmail } from "@/lib/users";
import { deleteUser } from "@/lib/users.server";
import { NextApiRequest, NextApiResponse } from "next";
import { getServerSession } from "next-auth";
import { authOptions } from "./auth/[...nextauth]";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<undefined>
) {
    const { method } = req;
    const session = await getServerSession(req, res, authOptions);
    const email = session?.user?.email;
    if (!email) {
        res.status(HttpStatus.FORBIDDEN).json(undefined);
        res.end();
        return;
    }

    if (method === "GET") {
        return reset(hashEmail(email).toString(), res);
    } else {
        res.setHeader("Allow", ["GET"]);
        res.status(HttpStatus.METHOD_NOT_ALLOWED).end(`Method ${method} not allowed`);
        res.end();
        return Promise.resolve();
    }
}

async function reset(emailHash: string, res: NextApiResponse<undefined>) {
    await deleteScans(emailHash);
    await deleteUser(emailHash);
    res.redirect("/");
    res.end();
    return;
}