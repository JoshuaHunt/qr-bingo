import { HttpStatus } from '@/lib/HttpStatus';
import { Scan } from '@/lib/Scan';
import { getScans } from '@/lib/scans.server';
import { hashEmail } from '@/lib/users';
import { getUser } from '@/lib/users.server';
import type { NextApiRequest, NextApiResponse } from 'next'
import { getServerSession } from 'next-auth';
import { authOptions } from './auth/[...nextauth]';


export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Scan[] | undefined>
) {
    const { method } = req;
    
    const session = await getServerSession(req, res, authOptions);
    const scannerEmail = session?.user?.email;
    if (!scannerEmail) {
        res.status(HttpStatus.FORBIDDEN).json(undefined);
        res.end();
        return;
    }
    const scannerEmailHash = hashEmail(scannerEmail).toString();

    if (method === "GET") {
        return handleGet(res, scannerEmailHash);
    } else {
        res.setHeader('Allow', ['GET']);
        res.status(HttpStatus.METHOD_NOT_ALLOWED).end(`Method ${method} not allowed`);
        return Promise.resolve();
    }
}

async function handleGet(
    res: NextApiResponse<Scan[] | undefined>,
    scannerEmailHash: string,
) {
    const result = await getScans(scannerEmailHash);
    if (!result) {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(undefined);
        res.end();
        return
    }
    const resultsWithNames = await Promise.all(result.map(async scan => {
        const scannee = await getUser(scan.scannee);
        return {scanneeName: scannee?.name, ...scan};
    }))
    res.status(HttpStatus.OK).json(resultsWithNames);
    res.end();
}