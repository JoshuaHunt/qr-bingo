import { BingoBoard } from "@/lib/BingoBoard";
import { generateBoardForUser, getBoardForUser } from "@/lib/bingoboards.server";
import { HttpStatus } from "@/lib/HttpStatus";
import { isAuthorised } from "@/lib/users.server";
import type { NextApiRequest, NextApiResponse } from "next"


export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<BingoBoard | undefined>
) {
    const { query, method, body } = req;
    const emailHash = query.id as string;

    if (!await isAuthorised(req, res, emailHash)) {
        return;
    }

    if (method === "GET") {
        return getBoard(res, emailHash);
    } else if (method === "POST") {
        return generateBoard(res, emailHash);
    } else {
        res.setHeader("Allow", ["GET", "POST"]);
        res.status(HttpStatus.METHOD_NOT_ALLOWED).end(`Method ${method} not allowed`);
        res.end();
        return Promise.resolve();
    }
}

async function getBoard(res: NextApiResponse<BingoBoard | undefined>, emailHash: string) {
    const result = await getBoardForUser(emailHash);
    if (result) {
        res.status(HttpStatus.OK).json(result);
    } else {
        res.status(HttpStatus.NOT_FOUND).json(undefined);
    }
    res.end();
}

async function generateBoard(
    res: NextApiResponse<BingoBoard | undefined>,
    emailHash: string,
) {
    const result = await generateBoardForUser(emailHash);
    if (result) {
        return getBoard(res, emailHash);
    } else {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(undefined);
        res.end();
    }
}