// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { getUser, createUser, isAuthorised } from '@/lib/users.server';
import { Age, Belt, Eyes, FacialHair, Gender, Gi, Hair, Tattoo, toAge, toBelt, toEyes, toFacialHair, toGender, toGiColour, toHair, toTattoo, User } from '@/lib/User';
import { HttpStatus } from '@/lib/HttpStatus';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<User | undefined>
) {
  const { query, method, body } = req;
  const emailHash = query.id as string;

  if (!await isAuthorised(req, res, emailHash)) {
    return Promise.resolve();
  }

  switch (method) {
    case "GET":
      return handleGet(res, emailHash);
    case "PUT":
      console.log(body.consent);
      console.log(String(body.consent).toLowerCase());
      // body.consent = "consent" if consented, empty otherwise.
      const consent = Boolean(body.consent);
      const user: User = {
        emailHash,
        name: body.name,
        belt: toBelt(body.belt) ?? Belt.UNKNOWN,
        gi: toGiColour(body.gi) ?? Gi.UNKNOWN,
        gender: toGender(body.gender) ?? Gender.UNKNOWN,
        age: toAge(body.age) ?? Age.UNKNOWN,
        eyes: toEyes(body.eyes) ?? Eyes.UNKNOWN,
        weight: body.weight ? parseInt(body.weight) : undefined,
        facialHair: toFacialHair(body.facialHair) ?? FacialHair.UNKNOWN,
        hair: toHair(body.hair) ?? Hair.UNKNOWN,
        tattoo: toTattoo(body.tattoo) ?? Tattoo.UNKNOWN,
      }
      return handlePut(res, user, consent);
    default:
      res.setHeader('Allow', ['GET', 'PUT']);
      res.status(HttpStatus.METHOD_NOT_ALLOWED).end(`Method ${method} not allowed`);
      res.end();
      return Promise.resolve();
  }
}

async function handleGet(res: NextApiResponse<User | undefined>, emailHash: string) {
  const result = await getUser(emailHash);
  if (result) {
    res.status(HttpStatus.OK).json(result);
  } else {
    res.status(HttpStatus.NOT_FOUND).json(undefined);
  }
  res.end();
}

async function handlePut(res: NextApiResponse<User | undefined>, user: Omit<User, "id">, consent: boolean) {
  if (!consent) {
    // client did not tick the "consent" box
    res.status(HttpStatus.BAD_REQUEST).json(undefined);
    return;
  }
  const result = await createUser(user);
  if (result) {
    res.status(HttpStatus.CREATED).json({...user, id: result.insertedId});
  } else {
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(undefined);
  }
  res.end();
}