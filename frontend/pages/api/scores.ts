import { HttpStatus } from "@/lib/HttpStatus";
import { Score } from "@/lib/Score";
import { getScores } from "@/lib/scores.server";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Score[]>,
) {
    const { method } = req;

    if (method !== "GET") {
        res.setHeader('Allow', ['GET']);
        res.status(HttpStatus.METHOD_NOT_ALLOWED).end(`Method ${method} not allowed`);
    }

    const scores = await getScores();
    if (!scores) {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR);
        res.end();
        return;
    }
    res.status(HttpStatus.OK).json(scores);
    res.end();
}