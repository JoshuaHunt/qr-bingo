import { Scan } from "./Scan";
import { Collection, DeleteResult, FindCursor, InsertOneResult, WithId } from "mongodb";
import clientPromise from "./mongo";

async function getScansCollection(): Promise<Collection<Scan>> {
    const client = await clientPromise
    const db = client.db();
    return db.collection("scans");
}

export async function getAllScans(): Promise<WithId<Scan>[]> {
    const scans = await getScansCollection();
    return scans.find({}).toArray();
}

export async function getScans(scannerEmailHash: string): Promise<WithId<Scan>[]> {
    const scans = await getScansCollection();
    return scans.find({ scanner: scannerEmailHash }).toArray();
}

export async function createScan(scan: Omit<Scan, "id">): Promise<InsertOneResult<Scan>> {
    const scans = await getScansCollection();
    return scans.insertOne(scan);
}

export async function deleteScans(scannerEmailHash: string): Promise<DeleteResult> {
    const scans = await getScansCollection();
    return scans.deleteMany({ scanner: scannerEmailHash });
}