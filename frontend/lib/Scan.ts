import { ObjectId } from "mongodb";

export type Scan = {
    id?: ObjectId,
    // Scanner's email address hashed with the hashEmail method from @/lib/users.ts.
    scanner: string,
    // Scannee's email address hashed with the hashEmail method from @/lib/users.ts.
    scannee: string,
    // The username of the scannee.
    scanneeName?: string,
    // The property of the scannee that this scan was claiming (e.g. "younger than me", "blue
    // belt", etc.)
    property: string,
};