import { Age, Belt, Eyes, FacialHair, Gender, Gi, Hair, Tattoo, User } from "./User"

export type BingoProperty = {
    // The unique identifier for this property.
    key: string
    // A human-readable description of this property
    //
    // Examples: "Has a white belt", "Wearing a white gi", "Competing as female".
    description: string
    // The predicate that defines this property.
    //
    // Returns true if `scanned` and `scanner` (i.e. you) together satisfy this property.
    matches: (scanned: User, scanner: User) => boolean,
}

const bingoPropertiesList: BingoProperty[] = [
    {
        key: "bow-and-arrow",
        description: "Tapped to your bow and arrow choke",
        matches: _ => true
    },
    {
        key: "rear-naked",
        description: "Tapped to your rear naked choke",
        matches: _ => true
    },
    {
        key: "leg-triangle",
        description: "Tapped to your leg triangle",
        matches: _ => true
    },
    {
        key: "arm-triangle",
        description: "Tapped to your arm triangle",
        matches: _ => true
    },
    {
        key: "cross-choke",
        description: "Tapped to your cross choke",
        matches: _ => true
    },
    {
        key: "guillotine",
        description: "Tapped to your guillotine",
        matches: _ => true
    },
    {
        key: "d-arce",
        description: "Tapped to your D'arce choke",
        matches: _ => true
    },
    {
        key: "armbar",
        description: "Tapped to your armbar",
        matches: _ => true
    },
    {
        key: "whitebelt",
        description: "Has a white belt",
        matches: scanned => scanned.belt === Belt.WHITE
    },
    {
        key: "bluebelt",
        description: "Has a blue belt",
        matches: scanned => scanned.belt === Belt.BLUE
    },
    {
        key: "purplebelt",
        description: "Has a purple belt",
        matches: scanned => scanned.belt === Belt.PURPLE
    },
    {
        key: "brownbelt",
        description: "Has a brown belt",
        matches: scanned => scanned.belt === Belt.BROWN
    },
    {
        key: "blackbelt",
        description: "Has a black belt",
        matches: scanned => scanned.belt === Belt.BLACK
    },
    {
        key: "same-belt",
        description: "Has the same belt as you",
        matches: (scanned, you) => {
            return scanned.belt !== Belt.UNKNOWN
                && you.belt !== Belt.UNKNOWN
                && scanned.belt === you.belt
        },
    },
    {
        key: "different-belt",
        description: "Has a different belt to you",
        matches: (scanned, you) => {
            return scanned.belt !== Belt.UNKNOWN
                && you.belt !== Belt.UNKNOWN
                && scanned.belt !== you.belt;
        },
    },
    {
        key: "lower-belt",
        description: "Has a lower belt than you",
        matches: (scanned, you) => {
            return scanned.belt !== Belt.UNKNOWN
                && you.belt !== Belt.UNKNOWN
                && scanned.belt < you.belt;
        },
    },
    {
        key: "higher-belt",
        description: "Has a higher belt than you",
        matches: (scanned, you) => {
            return scanned.belt !== Belt.UNKNOWN
                && you.belt !== Belt.UNKNOWN
                && scanned.belt > you.belt;
        },
    },
    {
        key: "no-higher-belt",
        description: "Has the same or lower belt than you",
        matches: (scanned, you) => {
            return scanned.belt !== Belt.UNKNOWN
                && you.belt !== Belt.UNKNOWN
                && scanned.belt <= you.belt;
        },
    },
    {
        key: "no-lower-belt",
        description: "Has the same or higher belt than you",
        matches: (scanned, you) => {
            return scanned.belt !== Belt.UNKNOWN
                && you.belt !== Belt.UNKNOWN
                && scanned.belt >= you.belt;
        },
    },
    {
        key: "white-gi",
        description: "Is wearing a white gi",
        matches: scanned => scanned.gi === Gi.WHITE,
    },
    {
        key: "blue-gi",
        description: "Is wearing a blue gi",
        matches: scanned => scanned.gi === Gi.BLUE,
    },
    {
        key: "black-gi",
        description: "Is wearing a black gi",
        matches: scanned => scanned.gi === Gi.BLACK,
    },
    {
        key: "other-gi",
        description: "Is wearing a gi that's not white / blue / black",
        matches: scanned => scanned.gi === Gi.OTHER,
    },
    {
        key: "not-white-gi",
        description: "Is not wearing a white gi",
        matches: scanned => scanned.gi !== Gi.UNKNOWN && scanned.gi !== Gi.WHITE,
    },
    {
        key: "not-blue-gi",
        description: "Is not wearing a blue gi",
        matches: scanned => scanned.gi !== Gi.UNKNOWN && scanned.gi !== Gi.BLUE,
    },
    {
        key: "not-black-gi",
        description: "Is not wearing a black gi",
        matches: scanned => scanned.gi !== Gi.UNKNOWN && scanned.gi !== Gi.BLACK,
    },
    {
        key: "same-gi",
        description: "Is wearing the same colour gi as you",
        matches: (scanned, you) => {
            return scanned.gi !== Gi.UNKNOWN
                && you.gi !== Gi.UNKNOWN
                && scanned.gi === you.gi;
        }
    },
    {
        key: "different-gi",
        description: "Is wearing a different colour gi to you",
        matches: (scanned, you) => {
            return scanned.gi !== Gi.UNKNOWN
                && you.gi !== Gi.UNKNOWN
                && scanned.gi !== you.gi;
        }
    },
    {
        key: "male",
        description: "Competes as male",
        matches: scanned => scanned.gender === Gender.MALE,
    },
    {
        key: "female",
        description: "Competes as female",
        matches: scanned => scanned.gender === Gender.FEMALE,
    },
    {
        key: "same-gender",
        description: "Competes in the same gender division as you",
        matches: (scanned, you) => {
            return scanned.gender !== Gender.UNKNOWN
                && you.gender !== Gender.UNKNOWN
                && scanned.gender === you.gender;
        }
    },
    {
        key: "different-gender",
        description: "Competes in the opposite gender division to you",
        matches: (scanned, you) => {
            return scanned.gender !== Gender.UNKNOWN
                && you.gender !== Gender.UNKNOWN
                && scanned.gender !== you.gender;
        }
    },
    {
        key: "same-age",
        description: "Is the same age as you",
        matches: (scanned, you) => {
            return scanned.age !== Age.UNKNOWN
                && you.age !== Age.UNKNOWN
                && scanned.age === you.age;
        }
    },
    {
        key: "younger",
        description: "Is younger than you",
        matches: (scanned, you) => {
            return scanned.age !== Age.UNKNOWN
                && you.age !== Age.UNKNOWN
                && scanned.age < you.age;
        }
    },
    {
        key: "older",
        description: "Is older than you",
        matches: (scanned, you) => {
            return scanned.age !== Age.UNKNOWN
                && you.age !== Age.UNKNOWN
                && scanned.age > you.age;
        }
    },
    {
        key: "twenties",
        description: "Is in their 20s or younger",
        matches: scanned => scanned.age === Age.TWENTIES,
    },
    {
        key: "thirties",
        description: "Is in their 30s",
        matches: scanned => scanned.age === Age.THIRTIES,
    },
    {
        key: "fourties",
        description: "Is in their 40s",
        matches: scanned => scanned.age === Age.FOURTIES,
    },
    {
        key: "fifties",
        description: "Is in their 50s or older",
        matches: scanned => scanned.age === Age.FIFTIES,
    },
    {
        key: "less-10-lighter",
        description: "Is <10kg lighter than you",
        matches: (scanned, you) => {
            return !!scanned.weight
                && !!you.weight
                && you.weight - 10 <= scanned.weight
                && scanned.weight <= you.weight;
        }
    },
    {
        key: "more-10-lighter",
        description: "Is 10kg–20kg lighter than you",
        matches: (scanned, you) => {
            return !!scanned.weight
                && !!you.weight
                && you.weight - 20 <= scanned.weight
                && scanned.weight <= you.weight - 10;
        }
    },
    {
        key: "more-20-lighter",
        description: "Is 20kg–30kg lighter than you",
        matches: (scanned, you) => {
            return !!scanned.weight
                && !!you.weight
                && you.weight - 30 <= scanned.weight
                && scanned.weight <= you.weight - 20;
        }
    },
    {
        key: "more-30-lighter",
        description: "Is 30kg–40kg lighter than you",
        matches: (scanned, you) => {
            return !!scanned.weight
                && !!you.weight
                && you.weight - 40 <= scanned.weight
                && scanned.weight <= you.weight - 30;
        }
    },
    {
        key: "more-40-lighter",
        description: "Is ≥40kg lighter than you",
        matches: (scanned, you) => {
            return !!scanned.weight
                && !!you.weight
                && scanned.weight <= you.weight - 40;
        }
    },
    {
        key: "less-10-heavier",
        description: "Is <10kg heavier than you",
        matches: (scanned, you) => {
            return !!scanned.weight
                && !!you.weight
                && you.weight <= scanned.weight
                && scanned.weight <= you.weight + 10;
        }
    },
    {
        key: "more-10-heavier",
        description: "Is 10kg–20kg heavier than you",
        matches: (scanned, you) => {
            return !!scanned.weight
                && !!you.weight
                && you.weight + 10 <= scanned.weight
                && scanned.weight <= you.weight + 20;
        }
    },
    {
        key: "more-20-heavier",
        description: "Is 20kg–30kg heavier than you",
        matches: (scanned, you) => {
            return !!scanned.weight
                && !!you.weight
                && you.weight + 20 <= scanned.weight
                && scanned.weight <= you.weight + 30;
        }
    },
    {
        key: "more-30-heavier",
        description: "Is 30kg–40kg heavier than you",
        matches: (scanned, you) => {
            return !!scanned.weight
                && !!you.weight
                && you.weight + 30 <= scanned.weight
                && scanned.weight <= you.weight + 40;
        }
    },
    {
        key: "more-40-heavier",
        description: "Is ≥40kg heavier than you",
        matches: (scanned, you) => {
            return !!scanned.weight
                && !!you.weight
                && you.weight + 40 <= scanned.weight;
        }
    },
    {
        key: "blue-eyes",
        description: "Has blue eyes",
        matches: scanned => scanned.eyes === Eyes.BLUE,
    },
    {
        key: "green-eyes",
        description: "Has green eyes",
        matches: scanned => scanned.eyes === Eyes.GREEN,
    },
    {
        key: "brown-eyes",
        description: "Has brown eyes",
        matches: scanned => scanned.eyes === Eyes.BROWN,
    },
    {
        key: "other-eyes",
        description: "Has neither blue, brown, nor green eyes",
        matches: scanned => scanned.eyes === Eyes.OTHER,
    },
    {
        key: "not-blue-eyes",
        description: "Does not have blue eyes",
        matches: scanned => scanned.eyes !== Eyes.UNKNOWN && scanned.eyes !== Eyes.BLUE,
    },
    {
        key: "not-green-eyes",
        description: "Does not have green eyes",
        matches: scanned => scanned.eyes !== Eyes.UNKNOWN && scanned.eyes !== Eyes.GREEN,
    },
    {
        key: "not-brown-eyes",
        description: "Does not have brown eyes",
        matches: scanned => scanned.eyes !== Eyes.UNKNOWN && scanned.eyes !== Eyes.BROWN,
    },
    {
        key: "no-facial-hair",
        description: "Has no facial hair",
        matches: scanned => scanned.facialHair === FacialHair.NONE,
    },
    {
        key: "moustache",
        description: "Has a moustache",
        matches: scanned => scanned.facialHair === FacialHair.MOUSTACHE,
    },
    {
        key: "beard",
        description: "Has a beard",
        matches: scanned => scanned.facialHair === FacialHair.BEARD,
    },
    {
        key: "no-hair",
        description: "Has no head hair",
        matches: scanned => scanned.hair === Hair.NONE,
    },
    {
        key: "short-hair",
        description: "Has shorter than shoulder-length hair",
        matches: scanned => scanned.hair === Hair.SHORT,
    },
    {
        key: "long-hair",
        description: "Has longer than shoulder-length hair",
        matches: scanned => scanned.hair === Hair.LONG,
    },
    {
        key: "no-tattoo",
        description: "Has no tattoos",
        matches: scanned => scanned.tattoo === Tattoo.NONE,
    },
    {
        key: "multiple-tattoos",
        description: "Has more than one tattoo",
        matches: scanned => scanned.tattoo === Tattoo.MULTIPLE,
    },
    {
        key: "one-tattoo",
        description: "Has only one tattoo",
        matches: scanned => {
            return scanned.tattoo !== Tattoo.UNKNOWN
                && scanned.tattoo !== Tattoo.MULTIPLE
                && scanned.tattoo !== Tattoo.NONE;
        }
    },
    {
        key: "animal-tattoo",
        description: "Has a tattoo of an animal",
        matches: scanned => scanned.tattoo === Tattoo.ANIMAL,
    },
    {
        key: "flower-tattoo",
        description: "Has a tattoo of a flower",
        matches: scanned => scanned.tattoo === Tattoo.FLOWER,
    },
    {
        key: "name-tattoo",
        description: "Has a tattoo of a name",
        matches: scanned => scanned.tattoo === Tattoo.NAME,
    },
    {
        key: "skull-tattoo",
        description: "Has a tattoo of a skull",
        matches: scanned => scanned.tattoo === Tattoo.SKULL,
    },
    {
        key: "heart-tattoo",
        description: "Has a tattoo of a heart",
        matches: scanned => scanned.tattoo === Tattoo.HEART,
    },
]

export const bingoProperties = Object.fromEntries(bingoPropertiesList.map(x => [x.key, x]));

export type BingoBoard = {
    width: number,
    height: number,
    // A multi-dimensional array containing keys of BingoProperties.
    //
    // The widths and height of the array are given by `width` and `height`.
    // TODO: can we make this type-safer?
    squares: string[][]
}