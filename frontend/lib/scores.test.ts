import { describe, expect, test } from '@jest/globals';
import { Scan } from './Scan';
import { buildScores } from './scores';
import { Age, Belt, Eyes, FacialHair, Gender, Gi, Hair, Tattoo, User } from "./User";

function userWithName(name: string): User {
    return {
        emailHash: name,
        name,
        belt: Belt.UNKNOWN,
        gi: Gi.UNKNOWN,
        gender: Gender.UNKNOWN,
        age: Age.UNKNOWN,
        weight: undefined,
        eyes: Eyes.UNKNOWN,
        facialHair: FacialHair.UNKNOWN,
        hair: Hair.UNKNOWN,
        tattoo: Tattoo.UNKNOWN,
    }
}

describe('buildScores', () => {
    test('counts squares completed', () => {
        const users: User[] = [{
            ...userWithName("bob"),
            emailHash: "bob-hash",
            bingoBoard: {
                width: 1,
                height: 1,
                squares: [["foo"]],
            },
        }];
        const scans: Scan[] = [{
            scanner: "bob-hash",
            scannee: "jane-hash",
            property: "foo",
        }];

        const scores = buildScores(users, scans);

        expect(scores).toStrictEqual([{
            username: "bob",
            squaresCompleted: 1,
            rowsCompleted: 2,
            score: 21,
        }]);
    });

    test('counts rows completed', () => {
        const users: User[] = [{
            ...userWithName("bob"),
            emailHash: "bob-hash",
            bingoBoard: {
                width: 2,
                height: 2,
                squares: [["a", "b"], ["c", "d"]],
            },
        }];
        const scans: Scan[] = [{
            scanner: "bob-hash",
            scannee: "jane-hash",
            property: "c",
        },
        {
            scanner: "bob-hash",
            scannee: "jane-hash",
            property: "d",
        }];

        const scores = buildScores(users, scans);

        expect(scores).toStrictEqual([{
            username: "bob",
            squaresCompleted: 2,
            rowsCompleted: 1,
            score: 12,
        }]);
    });

    test('counts cols completed', () => {
        const users: User[] = [{
            ...userWithName("bob"),
            emailHash: "bob-hash",
            bingoBoard: {
                width: 2,
                height: 2,
                squares: [["a", "b"], ["c", "d"]],
            },
        }];
        const scans: Scan[] = [{
            scanner: "bob-hash",
            scannee: "jane-hash",
            property: "b",
        },
        {
            scanner: "bob-hash",
            scannee: "jane-hash",
            property: "d",
        }];

        const scores = buildScores(users, scans);

        expect(scores).toStrictEqual([{
            username: "bob",
            squaresCompleted: 2,
            rowsCompleted: 1,
            score: 12,
        }]);
    });
})