import { getAllScans } from "./scans.server";
import { getAllUsers } from "./users.server";
import { Score } from "./Score";
import { buildScores } from "./scores";

export async function getScores(): Promise<Score[]> {
    const [users, scans] = await Promise.all([getAllUsers(), getAllScans()]);
    return buildScores(users, scans);
}