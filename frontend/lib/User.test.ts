import { describe, expect, test } from '@jest/globals';
import { Belt, toBelt } from "./User";

describe('toBelt', () => {
    test('converts a lowercase belt', () => {
        expect(toBelt('blue')).toBe(Belt.BLUE);
    });

    test('converts an uppercase belt', () => {
        expect(toBelt('PURPLE')).toBe(Belt.PURPLE);
    })

    test('returns undefined for an unknown belt', () => {
        expect(toBelt('fuschia')).toBeUndefined();
    })
})