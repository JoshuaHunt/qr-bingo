import { describe, expect, test } from '@jest/globals';
import { bingoProperties } from './BingoBoard';
import { Age, Belt, Eyes, FacialHair, Gender, Gi, Hair, Tattoo, User } from "./User";

function userWithName(name: string): User {
    return {
        emailHash: name,
        name,
        belt: Belt.UNKNOWN,
        gi: Gi.UNKNOWN,
        gender: Gender.UNKNOWN,
        age: Age.UNKNOWN,
        weight: undefined,
        eyes: Eyes.UNKNOWN,
        facialHair: FacialHair.UNKNOWN,
        hair: Hair.UNKNOWN,
        tattoo: Tattoo.UNKNOWN,
    }
}

describe('higher-belt', () => {
    test('white is lower than blue', () => {
        const whiteBelt: User = {
            ...userWithName("white"),
            belt: Belt.WHITE,
        }
        const blueBelt: User = {
            ...userWithName("blue"),
            belt: Belt.BLUE,
        }
        const matcher = bingoProperties["higher-belt"].matches;

        expect(matcher(whiteBelt, blueBelt)).toBe(false);
        expect(matcher(blueBelt, whiteBelt)).toBe(true);
    });

    test('blue is lower than black', () => {
        const blackBelt: User = {
            ...userWithName("black"),
            belt: Belt.BLACK,
        }
        const blueBelt: User = {
            ...userWithName("blue"),
            belt: Belt.BLUE,
        }
        const matcher = bingoProperties["higher-belt"].matches;

        expect(matcher(blueBelt, blackBelt)).toBe(false);
        expect(matcher(blackBelt, blueBelt)).toBe(true);
    });

    test('same belt is not lower', () => {
        const purpleBelt: User = {
            ...userWithName("purple"),
            belt: Belt.PURPLE,
        }
        const matcher = bingoProperties["higher-belt"].matches;

        expect(matcher(purpleBelt, purpleBelt)).toBe(false);
    })

    test('unknown is not lower', () => {
        const whiteBelt: User = {
            ...userWithName("white"),
            belt: Belt.WHITE,
        }
        const noBelt = userWithName("unknown");
        const matcher = bingoProperties["higher-belt"].matches;

        expect(matcher(noBelt, whiteBelt)).toBe(false);
        expect(matcher(whiteBelt, noBelt)).toBe(false);
    })
})