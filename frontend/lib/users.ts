import murmurhash from "murmurhash";

export function hashEmail(email: string) {
    if (!process.env.NEXT_PUBLIC_EMAIL_HASH_SEED) {
        throw new Error("Missing environment variable NEXT_PUBLIC_EMAIL_HASH_SEED");
    }
    const seed = parseInt(process.env.NEXT_PUBLIC_EMAIL_HASH_SEED);
    return murmurhash(email, seed);
}