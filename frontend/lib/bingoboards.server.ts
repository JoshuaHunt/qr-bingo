import { UpdateResult } from "mongodb";
import { BingoBoard, bingoProperties } from "./BingoBoard";
import { User } from "./User";
import { getUser, getUsersCollection } from "@/lib/users.server";

function generateBoard(): BingoBoard {
    const boardSize = 5;
    const availableKeys = Object.keys(bingoProperties);
    // TODO: only include properties that the scanner can hope to match
    const squares: string[][] = []
    for (let x = 0; x < boardSize; x++) {
        squares.push([])
        for(let y = 0; y < boardSize; y++) {
            const i = Math.floor(Math.random() * availableKeys.length);
            const newKey = availableKeys[i];
            availableKeys.splice(i, 1);
            squares[x].push(newKey);
        }
    }
    return {width: boardSize, height: boardSize, squares};
}

export async function getBoardForUser(emailHash: string): Promise<BingoBoard | undefined> {
    const user = await getUser(emailHash);
    if (!user) {
        return Promise.reject(`Could not get user ${emailHash}`);
    }
    return user.bingoBoard;
}

/** Generates a bingo board and saves it in the database with the user. */
export async function generateBoardForUser(emailHash: string): Promise<UpdateResult> {
    const user = await getUser(emailHash);
    if (!user) {
        return Promise.reject(`Could not get user ${emailHash}`);
    }
    const userQuery = {_id: user._id }
    const board = generateBoard();
    const updatedUser: User = {...user, bingoBoard: board};

    const users = await getUsersCollection();
    const result = await users.updateOne(userQuery, {$set: updatedUser});

    return result;
}