export type Score = {
    username: string,
    squaresCompleted: number,
    rowsCompleted: number,
    score: number,
}
