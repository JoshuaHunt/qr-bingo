import { User } from "./User";
import { Scan } from "./Scan";
import { Score } from "./Score";

function computeScore(user: User, scans: Scan[]): Omit<Score, "username"> {
    const scanSet = Object.fromEntries(scans.map(scan => [scan.property, true]));
    const board = user.bingoBoard;
    if (!board) {
        return { squaresCompleted: 0, rowsCompleted: 0, score: 0 };
    }
    const squaresCompleted = board.squares.flat().filter(id => scanSet[id] === true).length;
    let rowsCompleted = 0;
    for (let row = 0; row < board.height; row++) {
        const complete = board.squares[row].every(property => scanSet[property] === true);
        if (complete) {
            rowsCompleted++;
        }
    }
    for (let col = 0; col < board.width; col++) {
        let complete = true;
        for (let row = 0; row < board.height; row++) {
            complete = complete && (scanSet[board.squares[row][col]] === true);
        }
        if (complete) {
            rowsCompleted++;
        }
    }
    return {
        squaresCompleted,
        rowsCompleted,
        score: rowsCompleted * 10 + squaresCompleted
    }
}

export function buildScores(users: User[], scans: Scan[]): Score[] {
    const scansById: { [key: string]: Scan[] } =
        Object.fromEntries(users.map(user => [user.emailHash, []]));
    for (const scan of scans) {
        scansById[scan.scanner].push(scan);
    }

    return users.map(user => {
        return {
            ...computeScore(user, scansById[user.emailHash]),
            username: user.name,
        }
    })
}