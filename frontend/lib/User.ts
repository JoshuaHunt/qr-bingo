import { ObjectId } from "mongodb";
import { BingoBoard } from "./BingoBoard";

export enum Belt {
    UNKNOWN,
    WHITE,
    BLUE,
    PURPLE,
    BROWN,
    BLACK,
}

export function toBelt(beltName: string | undefined): Belt | undefined {
    if (!beltName) {
        return undefined;
    }
    return Belt[beltName.toUpperCase() as keyof typeof Belt];
}

export enum Gi {
    UNKNOWN,
    WHITE,
    BLUE,
    BLACK,
    OTHER,
}

export function toGiColour(colour: string | undefined): Gi | undefined {
    if (!colour) {
        return undefined;
    }
    return Gi[colour.toUpperCase() as keyof typeof Gi];
}

export enum Gender {
    UNKNOWN,
    MALE,
    FEMALE,
}

export function toGender(gender: string | undefined): Gender | undefined {
    if (!gender) {
        return undefined;
    }
    return Gender[gender.toUpperCase() as keyof typeof Gender];
}

export enum Age {
    UNKNOWN,
    TWENTIES,
    THIRTIES,
    FOURTIES,
    FIFTIES,
}

export function toAge(age: string | undefined): Age | undefined {
    if (!age) {
        return undefined;
    }
    return Age[age.toUpperCase() as keyof typeof Age];
}

export enum Eyes {
    UNKNOWN,
    BLUE,
    GREEN,
    BROWN,
    OTHER,
}

export function toEyes(eyes: string | undefined): Eyes | undefined {
    if (!eyes) {
        return undefined;
    }
    return Eyes[eyes.toUpperCase() as keyof typeof Eyes];
}

export enum FacialHair {
    UNKNOWN,
    NONE,
    MOUSTACHE,
    BEARD,
}

export function toFacialHair(hair: string | undefined): FacialHair | undefined {
    if (!hair) {
        return undefined;
    }
    return FacialHair[hair.toUpperCase() as keyof typeof FacialHair];
}

export enum Hair {
    UNKNOWN,
    NONE,
    SHORT,
    LONG,
}

export function toHair(hair: string | undefined): Hair | undefined {
    if (!hair) {
        return undefined;
    }
    return Hair[hair.toUpperCase() as keyof typeof Hair];
}

export enum Tattoo {
    UNKNOWN,
    NONE,
    MULTIPLE,
    FLOWER,
    ANIMAL,
    NAME,
    SKULL,
    HEART,
    OTHER,
}

export function toTattoo(tattoo: string | undefined): Tattoo | undefined {
    if (!tattoo) {
        return undefined;
    }
    return Tattoo[tattoo.toUpperCase() as keyof typeof Tattoo];
}

export type User = {
    id?: ObjectId,
    // User's email address hashed with the hashEmail method from @/lib/users.ts.
    emailHash: string,
    name: string,

    // The user's bingo board.
    bingoBoard?: BingoBoard,

    belt: Belt,
    gi: Gi,
    gender: Gender,
    age: Age,
    // Weight in kg.
    weight?: number | null,
    eyes: Eyes,
    facialHair: FacialHair,
    hair: Hair,
    tattoo: Tattoo,
};