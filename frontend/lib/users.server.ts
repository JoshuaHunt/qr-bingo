import { Collection, DeleteResult, InsertOneResult, WithId } from "mongodb";
import clientPromise from "./mongo";
import { User } from "./User";
import type { NextApiRequest, NextApiResponse } from 'next'
import { getServerSession } from 'next-auth';
import { authOptions } from "@/pages/api/auth/[...nextauth]"
import { hashEmail } from '@/lib/users';
import { HttpStatus } from "./HttpStatus";

export async function getUsersCollection(): Promise<Collection<User>> {
    const client = await clientPromise
    const db = client.db();
    return db.collection("userData");
}

export async function getAllUsers(): Promise<WithId<User>[]> {
    const users = await getUsersCollection();
    return users.find({}).toArray();
}

export async function getUser(emailHash: string): Promise<WithId<User> | null> {
    const users = await getUsersCollection();
    return users.findOne({ emailHash });
}

export async function createUser(user: Omit<User, "id">): Promise<InsertOneResult<User>> {
    const users = await getUsersCollection();
    return users.insertOne(user);
}

export async function deleteUser(emailHash: string): Promise<DeleteResult> {
    const users = await getUsersCollection();
    return users.deleteMany({emailHash});
}

/**
 * Returns whether the email in the user's session matches emailHash.
 * 
 * If the hashes do not match, then isAuthorised will send a FORBIDDEN response to the request.
 */
export async function isAuthorised<T>(
    req: NextApiRequest,
    res: NextApiResponse<T | undefined>,
    emailHash: string
): Promise<boolean> {
    const session = await getServerSession(req, res, authOptions);
    const email = session?.user?.email;
    if (email && hashEmail(email).toString() === emailHash) {
        return true;
    }
    res.status(HttpStatus.FORBIDDEN).json(undefined);
    res.end();
    return false;
}