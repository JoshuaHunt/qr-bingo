'use client';

import { User } from "./User";
import useSWR from "swr";
import { hashEmail } from "./users";
import { usePathname, useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { SessionContextValue } from "next-auth/react";

type UserLookup = {
    user: User,
    error: undefined,
} | {
    user: undefined,
    error: "unknown" | "loading",
}

export function useUser(sessionLookup : SessionContextValue<true>): UserLookup {
    const {status: sessionStatus, data: session} = sessionLookup;
    const email = session?.user?.email;
    const { data: user, error, isLoading: isUserLoading } = useSWR(
        email ? "/api/user" : null,
        async (): Promise<User | undefined> => {
            if (!email) {
                // This should never actually happen, because if email is falsy then we pass `null`
                // as the key to useSWR
                throw new Error("This shouldn't happen™");
            }
            // TODO: sanitise email before hashing
            const response = await fetch(
                `/api/user/${hashEmail(email)}`,
                {
                    method: 'GET',
                    mode: 'same-origin',
                    credentials: 'same-origin',
                    headers: { 'Content-type': 'application/json' },
                });
            if (!response.ok) {
                throw new Error(response.statusText);
            }
            const payload: User = await response.json();
            return payload;
        }
    );

    if (sessionStatus === "loading" || isUserLoading) {
        return { user: undefined, error: "loading" };
    } else if (error) {
        return { user: undefined, error: "unknown" }
    } else if (!user) {
        return { user: undefined, error: "loading" };
    }

    return { user: user, error: undefined };
}

/**
 * Checks whether the user has been signed up, redirecting to /signup if not.
 * 
 * @param userLookup the value returned from useUser()
 * @returns a bool indicating whether to render the page.
 */
export function useSignedUpGuard(userLookup: UserLookup) {
    const router = useRouter();
    const [signedUp, setSignedUp] = useState(false);
    const url = usePathname();

    const {user, error} = userLookup;

    useEffect(() => {
        if (user?.name) {
            setSignedUp(true);
        } else if (error === "loading") {
            // don't redirect yet, we're waiting for the user data to load.
        } else if (url !== "/signup") {
            router.push("/signup");
        }
    }, [user, error, url, router])

    if (!signedUp || error) {
        // prevents a flicker of protected content
        return false;
    }

    return true;
}