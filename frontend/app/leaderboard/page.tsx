'use client'

import { Score } from "@/lib/Score";
import { useSignedUpGuard, useUser } from "@/lib/users.client";
import { useSession } from "next-auth/react";
import { ReactElement } from "react";
import useSWR from "swr";
import NavBar from "../components/NavBar";

export default function Leaderboard() {
    const sessionLookup = useSession({ required: true });
    const userLookup = useUser(sessionLookup);
    const isSignedUp = useSignedUpGuard(userLookup);
    const { user } = userLookup;
    const { status: sessionStatus } = sessionLookup;
    const { data: scores } = useSWR(
        "/api/scans", // Scores change whenever scans change
        async (): Promise<Score[]> => {
            const response = await fetch(
                "/api/scores",
                {
                    method: "GET",
                    mode: "same-origin",
                    credentials: "same-origin",
                    headers: { "Content-type": "application/json" },
                })
            return response.json();
        }
    );


    if (sessionStatus === "loading" || !isSignedUp || !user) {
        return null;
    }

    return <>
        <header>
            <NavBar />
            <h1>Leaderboard</h1>
        </header>
        <main>
            <p>
                You score 1 point for each square you scan and 10 points for each row/column
                that you complete.
            </p>
            <ScoresTable scores={scores} />
        </main>
    </>;
}

function ScoresTable(props: { scores: Score[] | undefined }): ReactElement | null {
    const scores = props.scores;
    if (scores === undefined) {
        return null;
    }
    scores.sort(descending);
    const rows =
        scores
            .filter(score => score.score > 0)
            .map(score => {
                return <tr key={score.username}>
                    <td>{score.username}</td>
                    <td>{score.squaresCompleted}</td>
                    <td>{score.rowsCompleted}</td>
                    <td>{score.score}</td>
                </tr>;
            });
    return (
        <table>
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Squares scanned</th>
                    <th>Rows completed</th>
                    <th>Points</th>
                </tr>
            </thead>
            <tbody>
                {rows}
            </tbody>
        </table>
    );
}

function descending(a: Score, b: Score): number {
    if (a.score > b.score) {
        return -1;
    } else if (a.score < b.score) {
        return 1;
    } else {
        return 0;
    }
}