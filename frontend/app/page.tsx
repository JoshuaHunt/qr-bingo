'use client';

import styles from './page.module.css'
import { useSignedUpGuard, useUser } from '@/lib/users.client';
import { signOut, useSession } from 'next-auth/react';
import { useRouter, useSearchParams } from 'next/navigation';
import { bingoProperties } from '@/lib/BingoBoard';
import NavBar from './components/NavBar';

function deleteAllData() {
  return fetch(`/api/reset`, {
    method: "GET",
    mode: 'same-origin',
    credentials: 'same-origin',
  })
}

export default function Home() {
  const sessionLookup = useSession({ required: true });
  const userLookup = useUser(sessionLookup);
  const isSignedUp = useSignedUpGuard(userLookup);
  const searchParams = useSearchParams();
  const router = useRouter();
  const { user } = userLookup;
  const { status: sessionStatus } = sessionLookup;

  if (sessionStatus == "loading" || !isSignedUp) {
    // Prevent flicker of content.
    return null;
  }

  const scannedProperty = searchParams.get("scanned");
  const scanSuccessful =
    scannedProperty
      ? <p>Successfully completed the square '{bingoProperties[scannedProperty].description}'</p>
      : null;

  return (
    <>
      <header>
        <NavBar />
        <h1>Welcome to BJJ Bingo!</h1>
      </header>
      <main className={styles.main}>
        <div className={styles.block}>
          <p>Hello, {user?.name}!</p>
          {scanSuccessful}
        </div>
        <div className={styles.block}>
          <h2>How to play</h2>
          <ol>
            <li>Click "Your bingo board" (at the top of the page) to view your personal bingo board.</li>
            <li>Choose a square that you want to complete.</li>
            <li>Find someone who matches that square, then roll with them.</li>
            <li>Click on the square that you want to complete, then scan your partner's QR code.</li>
            <li>Click "Your QR Code" (at the top of the page) to allow your partner to scan one of your squares.</li>
            <li>Keep completing squares, and check the leaderboard to see who's winning!</li>
          </ol>
        </div>

        <div className={styles.block}>
          <h2>Change or delete your data</h2>
          <button type="button" onClick={() => deleteAllData().then(() => router.push("/signup"))}>
            Change my data
          </button>
          <p>
            Clicking the above button will clear your data, allowing you to choose different answers
            for the questions you answered while signing up. This will reset your bingo board, but
            will leave other people's boards unchanged.
          </p>
          <button type="button" onClick={() => deleteAllData().then(() => signOut())}>Delete all my data</button>
        </div>
      </main>
    </>
  )
}
