'use client'

import { useSignedUpGuard, useUser } from "@/lib/users.client";
import { useSession } from "next-auth/react";
import QRCode from "react-qr-code";
import NavBar from "../components/NavBar";
import styles from "./page.module.css";

export default function ScanMe() {
    const sessionLookup = useSession({ required: true });
    const userLookup = useUser(sessionLookup);
    const isSignedUp = useSignedUpGuard(userLookup);
    const { status: sessionStatus } = sessionLookup;
    const { user } = userLookup;

    if (sessionStatus == "loading" || !isSignedUp || !user) {
        return null;
    }

    return <>
        <header>
            <NavBar />
            <h1>Your QR code</h1>
        </header>
        <main className={styles.main}>
            <p>
                Allow someone to scan this QR code to be added to their bingo board. Check their
                screen to verify that they're scanning a square that matches you!
            </p>
            <div className={styles.qr}>
                <QRCode value={user.emailHash} />
            </div>
        </main>
    </>;
}