'use client'

import NavBar from "@/app/components/NavBar";
import { useSignedUpGuard, useUser } from "@/lib/users.client";
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { QrReader } from "react-qr-reader";
import { mutate } from "swr";

type RouteParams = {
    // The property that should be checked for the scanned person
    property: string
}

export default function ScanQr(props: { params: RouteParams }) {
    const sessionLookup = useSession({ required: true });
    const userLookup = useUser(sessionLookup);
    const isSignedUp = useSignedUpGuard(userLookup);
    const router = useRouter();
    const { status: sessionStatus } = sessionLookup;
    const { user } = userLookup;
    const [scanned, setScanned] = useState("");
    const { property } = props.params;

    useEffect(() => {
        if (scanned) {
            fetch(`/api/scan/${scanned}`, {
                method: "PUT",
                mode: 'same-origin',
                credentials: 'same-origin',
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    property
                })
            })
                .then(response => {
                    if (response.ok) {
                        mutate("/api/scans");
                        router.push(`/?scanned=${property}`);
                    } else {
                        router.push(`/bingo?failed=${property}`)
                    }
                });

        }
    }, [scanned]);

    if (sessionStatus == "loading" || !isSignedUp || !user) {
        return null;
    }

    return (
        <>
            <header>
                <NavBar />
                <h1>Scan a QR code</h1>
            </header>
            <main>
                <p>{scanned ? "Scan successful" : "Scanning..."}</p>
                <QrReader
                    onResult={(result, _) => {
                        if (result) {
                            const scanneeHash = result.getText();
                            setScanned(scanneeHash);
                        }
                    }}
                    constraints={{ facingMode: "environment" }}
                    containerStyle={{ width: "400px" }}
                />
            </main>
        </>
    )
}