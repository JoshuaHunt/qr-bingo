import { bingoProperties } from '@/lib/BingoBoard';

export default function ScanFailed(props: { property: string | null }) {
    if (!props.property) {
        return null;
    }
    return <p>
        Failed to complete the square '{bingoProperties[props.property].description}': are you
        sure your partner matches it? <br /><i>Note:</i> if your partner chose "Prefer not to say",
        then you will not be able to scan their QR code for that question.
    </p>;
}
