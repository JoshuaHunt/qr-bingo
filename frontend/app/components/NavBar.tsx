import Link from "next/link";
import { ReactElement } from "react";
import SignOut from "./SignOut";

export default function NavBar(): ReactElement {
    return <ul>
        <li><Link href={"/"}>Home</Link></li>
        <li><Link href={"/viewqr"}>Your QR code</Link></li>
        <li><Link href={"/bingo"}>Your bingo board</Link></li>
        <li><Link href={"/leaderboard"}>Leaderboard</Link></li>
        <li><SignOut /></li>
    </ul>;
}