export default function Head() {
  return (
    <>
      <title>QR Bingo</title>
      <meta content="width=device-width, initial-scale=1" name="viewport" />
      <meta name="description" content="A web app for playing bingo with other people." />
      <link rel="icon" href="/favicon.ico" />
    </>
  )
}
