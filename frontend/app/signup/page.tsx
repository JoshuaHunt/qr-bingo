'use client'

import { FormEvent, useEffect, useState } from 'react'
import { hashEmail } from '@/lib/users';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import { AppRouterInstance } from 'next/dist/shared/lib/app-router-context';
import { useUser } from '@/lib/users.client';
import { useSWRConfig } from 'swr';
import { ScopedMutator } from 'swr/_internal';
import SignOut from '../components/SignOut';
import styles from "./page.module.css";


function handleSubmit(email: string, router: AppRouterInstance, mutate: ScopedMutator) {
  return async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const form = event.currentTarget;
    const formData = new FormData(form);
    const formFields = [
      "name",
      "belt",
      "gi",
      "gender",
      "age",
      "weight",
      "eyes",
      "facialHair",
      "hair",
      "tattoo",
      "consent",
    ];

    await fetch(`/api/user/${hashEmail(email)}`, {
      method: "PUT",
      mode: 'same-origin',
      credentials: 'same-origin',
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(Object.fromEntries(formFields.map(key => [key, formData.get(key)]))),
    });
    // Signal to the useUser hook that it should try to redownload user information.
    mutate("/api/user");


    return router.push("/");
  }
}

export default function Signup() {
  const sessionLookup = useSession({ required: true });
  const { user, error } = useUser(sessionLookup);
  const router = useRouter();
  const [isSignedUp, setIsSignedUp] = useState(false);
  const { status: sessionStatus, data: session } = sessionLookup;
  const { mutate } = useSWRConfig()

  useEffect(() => {
    if (isSignedUp) {
      router.push("/");
    }
  }, [isSignedUp]);

  if (sessionStatus === "loading" || !session.user?.email || isSignedUp) {
    // Prevent content flicker
    return null;
  }

  if (!error && user.name) {
    setIsSignedUp(true);
    return null;
  }

  return (
    <main>
      <h1>BJJ Bingo sign up</h1>
      <p>
        <b>Important:</b> Almost all of the questions are optional. However, if you choose "Prefer
        not to say" for a question, then you won't be able to scan or be scanned for many of the
        squares. For example, if you don't tell us what belt you have, then you won't be able to
        scan the square "Has a higher belt" (or let someone else scan that square).
      </p>
      <form onSubmit={handleSubmit(session.user.email, router, mutate)}>
        <p>
          <label htmlFor="name" className={styles.required}>Who are you?</label>
          <input type="text" id="name" name="name" required />
        </p>
        <p>
          <label htmlFor="belt">What colour belt do you have?</label>
          <select name="belt" id="belt">
            <option value="undefined">Prefer not to say</option>
            <option value="white">White</option>
            <option value="blue">Blue</option>
            <option value="purple">Purple</option>
            <option value="brown">Brown</option>
            <option value="black">Black</option>
          </select>
        </p>
        <p>
          <label htmlFor="gi">What colour gi are you wearing?</label>
          <select name="gi" id="gi">
            <option value="undefined">Prefer not to say</option>
            <option value="white">White</option>
            <option value="blue">Blue</option>
            <option value="black">Black</option>
            <option value="other">Other</option>
          </select>
        </p>
        <p>
          <label htmlFor="gender">Which gender do you compete as?</label>
          <select name="gender" id="gender">
            <option value="undefined">Prefer not to say</option>
            <option value="female">Female</option>
            <option value="male">Male</option>
          </select>
        </p>
        <p>
          <label htmlFor="age">How old are you?</label>
          <select name="age" id="age">
            <option value="undefined">Prefer not to say</option>
            <option value="twenties">20s or younger</option>
            <option value="thirties">30s</option>
            <option value="fourties">40s</option>
            <option value="fifties">50s or older</option>
          </select>
        </p>
        <p>
          <label htmlFor="weight">How much do you weigh in kg?</label>
          <input type="number" name="weight" id="weight" />
        </p>
        <p>
          <label htmlFor='eyes'>What colour are your eyes?</label>
          <select name="eyes" id="eyes">
            <option value="undefined">Prefer not to say</option>
            <option value="blue">Blue</option>
            <option value="green">Green</option>
            <option value="brown">Brown</option>
            <option value="other">Other</option>
          </select>
        </p>
        <p>
          <label htmlFor='facialHair'>What sort of facial hair do you have?</label>
          <select name="facialHair" id="facialHair">
            <option value="undefined">Prefer not to say</option>
            <option value="none">None</option>
            <option value="moustache">Moustache only</option>
            <option value="beard">Beard</option>
          </select>
        </p>
        <p>
          <label htmlFor='hair'>How long is the hair on your head?</label>
          <select name="hair" id="hair">
            <option value="undefined">Prefer not to say</option>
            <option value="none">No hair</option>
            <option value="short">Shorter than shoulder length</option>
            <option value="long">Longer than shoulder length</option>
          </select>
        </p>
        <p>
          <label htmlFor='tattoo'>Do you have a tattoo?</label>
          <select name="tattoo" id="tattoo">
            <option value="undefined">Prefer not to say</option>
            <option value="none">No</option>
            <option value="multiple">Yes, I have several</option>
            <option value="flower">Yes, a flower</option>
            <option value="animal">Yes, an animal</option>
            <option value="name">Yes, a name</option>
            <option value="skull">Yes, a skull</option>
            <option value="heart">Yes, a heart</option>
            <option value="other">Yes, of something else</option>
          </select>
        </p>
        <p>
          <input type="checkbox" id="consent" name="consent" value="consent" required />
          <label htmlFor="consent" className={styles.required}>
            I'm okay with this website storing my data for up to 24h and sharing it only with the
            other people who are using the website during the bingo game.
          </label>
        </p>
        <input type="submit" value="Submit" />
        <p><span style={{ color: "red" }}>*</span> marks required fields</p>
      </form>
      <p>
        This website only uses cookies to allow you to log in.
      </p>
      <div style={{ height: "100px" }}></div>
      <SignOut />
    </main>
  )
}
