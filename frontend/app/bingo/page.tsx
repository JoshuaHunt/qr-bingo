"use client";

import { BingoBoard, bingoProperties, BingoProperty } from "@/lib/BingoBoard";
import { Scan } from "@/lib/Scan";
import { useSignedUpGuard, useUser } from "@/lib/users.client";
import { useSession } from "next-auth/react";
import Link from "next/link";
import useSWR from "swr";
import NavBar from "../components/NavBar";
import styles from "./page.module.css";
import { useSearchParams } from "next/navigation";
import ScanFailed from "../components/ScanFailed";


async function getBoard(emailHash: string, method: "GET" | "POST"): Promise<BingoBoard | undefined> {
    const response = await fetch(
        `/api/bingoboard/${emailHash}`,
        {
            method,
            mode: "same-origin",
            credentials: "same-origin",
            headers: { "Content-type": "application/json" },
        });

    if (!response.ok) {
        return undefined;
    }
    return response.json();
}

function BingoCell(props: { property: BingoProperty, scanned: boolean }) {
    if (props.property === undefined) {
        return null;
    }
    if (props.scanned) {
        return <div className={`${styles.boardItem} ${styles.scanned}`}>
            {props.property.description}
        </div>;
    } else {
        return <div className={`${styles.boardItem} ${styles.unscanned}`}>
            <Link
                href={`/scanqr/${props.property.key}`}
            >
                {props.property.description}
            </Link>
        </div>;
    }
}

export default function Bingo() {
    const sessionLookup = useSession({ required: true });
    const userLookup = useUser(sessionLookup);
    const isSignedUp = useSignedUpGuard(userLookup);
    const searchParams = useSearchParams();
    const { user } = userLookup;
    const { status: sessionStatus } = sessionLookup;

    const emailHash = user?.emailHash;
    const { data: board } = useSWR(
        emailHash ? "/api/bingoboard" : null,
        async (): Promise<BingoBoard | undefined> => {
            if (!emailHash) {
                // This should never actually happen, because if email is falsy then we pass `null`
                // as the key to useSWR
                throw new Error("This shouldn't happen™");
            }
            let board = await getBoard(emailHash, "GET");
            if (!board) {
                // Create a new board
                board = await getBoard(emailHash, "POST");
            }
            return board;
        }
    );
    const { data: scans } = useSWR(
        user ? `/api/scans` : null,
        async (): Promise<Scan[] | undefined> => {
            const response = await fetch(
                `/api/scans`,
                {
                    method: 'GET',
                    mode: 'same-origin',
                    credentials: 'same-origin',
                    headers: { 'Content-type': 'application/json' },
                });
            if (!response.ok) {
                throw new Error(response.statusText);
            }
            const scans: Scan[] = await response.json();
            return scans;
        }
    );

    if (sessionStatus == "loading" || !isSignedUp || !board) {
        // Prevent flicker of content.
        return null;
    }

    const isScanned = scans ? Object.fromEntries(scans.map(scan => [scan.property, true])) : {};

    const cells = board.squares.flatMap(row => {
        return row.map(cell => {
            return <BingoCell
                key={cell}
                property={bingoProperties[cell]}
                scanned={isScanned[cell]}
            />;
        });
    });
    return <>
        <header>
            <NavBar />
            <h1>Bingo Board</h1>
        </header>
        <main>
            <ScanFailed property={searchParams.get("failed")}/>
            <div className={styles.board}>{cells}</div>
        </main>
    </>
}