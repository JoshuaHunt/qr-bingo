#!/bin/bash
set -e

mongosh <<EOF
use $MONGO_INITDB_DATABASE
db.createUser({
	user: 'frontend',
	pwd: '$FRONTEND_PASSWORD',
	roles: [{role: "readWrite", db: "$MONGO_INITDB_DATABASE"}]
});
EOF
