# QR bingo

## Setup

In order to run the service, you will need to add some environment variables. You do this by creating secrets files, each of which is a list of key-value pairs in the format

```
KEY=foo
ANOTHER_KEY=bah
```

### Database

Create a `db/secrets` file containing the variables:

 * `MONGO_INITDB_ROOT_USERNAME` with the desired username of the root user
 * `MONGO_INITDB_ROOT_PASSWORD` with the desired password of the root user
 * `MONGO_INITDB_DATABASE` with the desired database name to create.
 * `FRONTEND_PASSWORD` with the password that the frontend should use to connect to the database.

These can be set to arbitrary values. You can use `openssl rand -base64 32` to generate a strong
password.

### Frontend

Create a `frontend/secrets` file containing the variables:

 * `EMAIL_SERVER` with a connection string in the format `smtp://username:password@smtpserver:port`.
 * `EMAIL_FROM` with the email address you would like login emails to be sent from.
 * `MONGODB_URI` with the connection string for the mongo database, in the format
   `mongodb://frontend:<password>@db:27017/bingo`. Here `<password>` must be the same value that you
   set for `FRONTEND_PASSWORD` in the database secrets file.
 * `NEXT_PUBLIC_EMAIL_HASH_SEED` with an integer for hashing emails. This will be exposed to the
   client and must be set to the value contained in `docker-compose.override.yml`.

To run the prod server locally, you will additionally need to add the variables:

 * `NEXTAUTH_SECRET` with a random string, for example the output of `openssl rand -base64 32`.
 * `NEXTAUTH_URL` with the URL where the website is hosted.

Example:

```
EMAIL_SERVER=smtp://foo@gmail.com:password@smtp.gmail.com:587
EMAIL_FROM=no-reply@example.com
NEXTAUTH_SECRET=abc123
```

## Usage

To bring up a copy of the dev environment, run `docker compose up --build`. 

## Debugging

### Frontend

To get a shell in the frontend container, run 

```sh
docker exec -it qr-bingo-frontend-1 sh
```

### mongo

To get a shell in the mongo container, run

```sh
docker exec -it qr-bingo-db-1 mongosh
```

while the containers are running. You will then need to authenticate with 

```
> use bingo;
> db.auth('frontend');
```

supplying the password that you chose in `db/secrets`.

## Support

File bugs at https://gitlab.com/JoshuaHunt/qr-bingo/-/issues.

## Contributing

Pull requests are welcome!

## License

This code is licensed under GPLv3.

Copyright (C) 2023 Joshua Hunt.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or4
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
